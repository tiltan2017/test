var InitClass = InitClass || {};

InitClass.Init = function () {

    this.game = new Phaser.Game(800, 600, Phaser.AUTO, 'main_canvas');

    this.game.state.add('preloadState', InitClass.preloadState);

    this.game.state.add('gameState', InitClass.gameState);

    this.game.state.start('preloadState');

}

InitClass.preloadState = {

    preload: function () {

        var style = {
            font: "bold 32px Arial",
            fill: "#48ea48"
        };

        this.loadingText = this.game.add.text(350, 300, '0%', style);

        this.game.load.image('background', 'assets/bg.jpg');

        this.game.load.image('ground_tile', 'assets/ground_tile.png');

        this.game.load.image('door', 'assets/door.png');

        this.game.load.atlas('enemy', 'assets/spritesheets_ori/export-sheets/Enamy-Walk.png', 'assets/spritesheets_ori/export-sheets/Enamy-Walk.json');

        this.game.load.atlas('kitty', 'assets/spritesheets_ori/export-sheets/Kitty_Walk.png', 'assets/spritesheets_ori/export-sheets/Kitty_Walk.json');

        this.game.load.onFileComplete.add(this.updateProgressBar, this)

        this.game.load.text('levelData1', './assets/levels/level1.json')

        this.game.load.text('levelData2', './assets/levels/level2.json')
    },
    updateProgressBar: function (progress, cacheID, success, filesloaded, totalfiles) {

        this.loadingText.text = progress + '%';
    },
    create: function () {

        this.game.state.start('gameState');
    }
}

InitClass.gameState = {

    init: function (currentLevel) {
        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        this.game.physics.arcade.gravity.y = 700;

        this.cursors = this.game.input.keyboard.createCursorKeys();

        this.spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        this.RUNNING_SPEED = 250;

        this.JUMPING_SPEED = 550;

        this.currentLevel = currentLevel ? currentLevel : 2;

        this.TOTAL_LEVELS = 2;


        console.log("current level:" + this.currentLevel);


    },


    create: function () {

        this.game.add.sprite(0, 0, 'background');

        this.levelData = JSON.parse(this.game.cache.getText('levelData' + this.currentLevel));

        this.tileData = this.levelData.tileData;

        this.door = this.game.add.sprite(this.levelData.goal.x, this.levelData.goal.y, 'door');

        this.game.physics.arcade.enable(this.door);

        this.door.body.allowGravity = false;

        this.door.body.immovable = true;

        this.door.body.collideWorldBounds = true;

        this.tileLayer = this.game.add.physicsGroup();

        for (var i = 0; i < this.tileData.length; i++) {

            var data = this.tileData[i];

            var startWidth = 0;

            var startHeight = 0;

            while (startHeight <= data.height) {

                while (startWidth < data.width) {

                    var t = this.game.add.sprite(data.x + startWidth, data.y + startHeight, 'ground_tile');

                    startWidth += 50;

                    this.tileLayer.add(t);

                    this.game.physics.arcade.enable(t);

                    t.body.allowGravity = false;

                    t.body.immovable = true;

                    t.body.collideWorldBounds = true;
                }

                startHeight += 50;

                startWidth = 0;

            }
        }

        /*** add enemies**/

        this.enemies = [];

        this.enemyData = this.levelData.enemyData;

        /*** add player**/

        this.player = this.game.add.sprite(140, 0, 'kitty');

        this.player.animations.add('walk');

        this.player.animations.play('walk', 20, true);

        this.player.anchor.setTo(0.5, 1);

        this.player.scale.x = -0.4;

        this.player.scale.y = 0.4;

        for (var i = 0; i < this.enemyData.length; i++) {

            var enemyData = this.enemyData[i];

            var enemy = this.game.add.sprite(enemyData.boundStartX, enemyData.boundStartY, 'enemy');

            enemy.animations.add('walk');

            enemy.animations.play('walk', 20, true);

            enemy.anchor.setTo(0.5, 1);

            enemy.scale.x = -0.4;

            enemy.scale.y = 0.4;

            this.game.physics.arcade.enable(enemy);

            enemy.body.collideWorldBounds = true;

            this.enemies.push(enemy);

        }

        this.game.physics.arcade.enable(this.player);

        this.player.body.collideWorldBounds = true;


    },
    update: function () {

        this.player.body.velocity.x = 0;


        if (this.cursors.left.isDown) {


            this.player.body.velocity.x -= this.RUNNING_SPEED;

            this.player.scale.x = 0.4;
        } else if (this.cursors.right.isDown) {
            this.player.body.velocity.x += this.RUNNING_SPEED;

            this.player.scale.x = -0.4;

        }


        this.game.physics.arcade.collide(this.player, this.tileLayer);

        if (this.spaceKey.isDown && this.player.body.touching.down) {


            this.player.body.velocity.y -= this.JUMPING_SPEED;
        }


        for (var j = 0; j < this.enemies.length; j++) {

            var enemyData = this.enemyData[j];

            var enemy = this.enemies[j];


            this.enemies[j].x += enemyData.speedX;

            this.game.physics.arcade.collide(enemy, this.tileLayer);

            this.game.physics.arcade.collide(enemy, this.player, function () {

                if (enemy.body.touching.up) {

                    enemy.destroy();

                    console.log("kill enemy!")
                }
            });


            if (this.enemies[j].x > enemyData.boundEndX || this.enemies[j].x < enemyData.boundStartX) {

                enemyData.speedX *= -1;

                this.enemies[j].scale.x *= -1;


            }

        }

        this.game.physics.arcade.overlap(this.player, this.door, this.goToNextLevel, null, this);
    },



    goToNextLevel: function () {


        console.log("loading next level..")

        this.currentLevel++;

        if (this.currentLevel > this.TOTAL_LEVELS)
            this.currentLevel = 1;

        this.game.state.start("gameState", true, false, this.currentLevel)
    }

}