var InitClass=InitClass||{};

InitClass.Init=function(){
    
    this.game = new Phaser.Game(800, 600, Phaser.AUTO, 'main_canvas', { init:init ,preload: preload, create: create, update: update });


    function init(){
        
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        
        this.game.physics.arcade.gravity.y=450;
        
        this.cursors=this.game.input.keyboard.createCursorKeys();
        
        this.spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        
        this.RUNNING_SPEED=250;
        
    }
    
    function preload() {

        //  37x45 is the size of each frame
        //  There are 18 frames in the PNG - you can leave this value blank if the frames fill up the entire PNG, but in this case there are some
        //  blank frames at the end, so we tell the loader how many to load
        
        var style = { font: "bold 32px Arial", fill: "#48ea48"};
        
        this.loadingText=this.game.add.text(350,300,'0%',style);
        
        this.game.load.image('background', 'assets/bg.jpg');
        
        this.game.load.image('ground_tile', 'assets/ground_tile.png'); 
        
        this.game.load.atlas('enemy', 'assets/spritesheets_ori/export-sheets/Enamy-Walk.png', 'assets/spritesheets_ori/export-sheets/Enamy-Walk.json');
        
        this.game.load.atlas('kitty', 'assets/spritesheets_ori/export-sheets/Kitty_Walk.png', 'assets/spritesheets_ori/export-sheets/Kitty_Walk.json');
        
        this.game.load.onFileComplete.add(updateProgressBar, this)
        
        this.game.load.text('levelData','./assets/levels/level1.json')
          

    }
    
    function updateProgressBar(progress, cacheID, success, filesloaded, totalfiles){
        
        this.loadingText.text=progress+'%';
    }

    function create() {

        this.game.add.sprite(0, 0, 'background');
        
        this.tiles=[];
        
        this.levelData=JSON.parse(this.game.cache.getText('levelData'));
        
        var ground_0=this.game.add.tileSprite(0, 550, 800, 50, 'ground_tile');
        
        var ground_1=this.game.add.tileSprite(300, 300, 150, 50, 'ground_tile');
    
        this.tiles.push(ground_0,ground_1);
       
        this.enemies=[];
               
        this.enemyData=this.levelData.enemyData;
                
        this.player = this.game.add.sprite(140, 300, 'kitty');

        this.player.animations.add('walk');

        this.player.animations.play('walk', 24, true);
        
        this.player.anchor.setTo(0.5);
        
        this.player.scale.x=-0.4;
        
        this.player.scale.y=0.4;
    
        for(var i=0; i<this.enemyData.length; i++){
            
            var enemyData=this.enemyData[i];
            
            var enemy = this.game.add.sprite(enemyData.boundStartX, enemyData.boundStartY, 'enemy');

            enemy.animations.add('walk');

            enemy.animations.play('walk', 20, true);

            enemy.anchor.setTo(0.5);

            enemy.scale.x=-0.4;

            enemy.scale.y=0.4;
            
            this.game.physics.arcade.enable(enemy);
            
            enemy.body.allowGravity=false;
            
            this.enemies.push(enemy);
        
        }
        
         for(var i=0;i<this.tiles.length;i++){
                
            this.game.physics.arcade.enable(this.tiles[i]);
             
            this.tiles[i].body.allowGravity=false;
             
            this.tiles[i].body.immovable=true;
    
            this.tiles[i].body.collideWorldBounds = true;
           
         }
        
        this.game.physics.arcade.enable(this.player);
        
        this.player.body.collideWorldBounds = true;
        

    }

    //  update isn't called until 'create' has completed. If you need to process stuff before that point (i.e. while the preload is still happening)
    //  then create a function called loadUpdate() and use that
    function update() {

        this.player.body.velocity.x=0;
        
        console.log(this.cursors.up.isDown);
        
        
        if(this.cursors.left.isDown){
            
            
            this.player.body.velocity.x-=this.RUNNING_SPEED;
            
            this.player.scale.x=0.4;
        }
        
        else if(this.cursors.right.isDown){
            this.player.body.velocity.x+=this.RUNNING_SPEED;
            
             this.player.scale.x=-0.4;
        
        }
    
        
        for(var i=0;i<this.tiles.length;i++){
        
          this.game.physics.arcade.collide(this.player,this.tiles[i],hitGround);
        
        }
        
        for(var i=0;i<this.enemies.length;i++){
            
            var enemyData=this.enemyData[i];
            
            this.enemies[i].x+=enemyData.speedX;
            
            if(this.enemies[i].x>=enemyData.boundEndX || this.enemies[i].x<=enemyData.boundStartX){
               
                enemyData.speedX*=-1;
                
                this.enemies[i].scale.x*=-1;
                
            
            }

            this.game.physics.arcade.collide(this.player, this.enemies[i], killPlayer); 
        }
        

    }
    
    function hitGround(){
        
        
    }

    function killPlayer(){
        console.log("you are dead");
    }
    
    
    
    
    
    
}