<<<<<<< HEAD
var MainGame=MainGame ||{};
=======
var InitGame=InitGame || {};
>>>>>>> d2f9a2feb20d141d80bd6b848cf4327980937814

InitGame.init=function(){
//var InitGame=function(){
    
    window.addEventListener("load", function() {

            var canvas = document.getElementById("mycanvas");

            var ctx = canvas.getContext("2d");

            //constants
            var GAME_WIDTH = 640;

            var GAME_HEIGHT = 360;

            var sprites = {
                player: null,
                bg: null
            };


            var loadAssets = function() {

                sprites.player = new Image();

                sprites.player.src = "assets/crab.png";

                sprites.bg = new Image();

                sprites.bg.src = "assets/beach.png";
            };


            //var enemies = [new Enemy(100, 0), new Enemy(200, 360), new Enemy(400, 100)];
            
            var enemies = [{
                    x: 100, //x coordinate
                    y: 100, //y coordinate
                    speedY: 2, //speed in Y
                    width: 40, //width
                    height: 40,
                    color: "#ffcc11",
                    changeColor: function(color) {

                        this.color = color;


                    } //heght
                },
                {
                    x: 200,
                    y: 0,
                    speedY: 2,
                    width: 40,
                    height: 40,
                    color: "#ffcc11",
                    changeColor: function(color) {

                        this.color = color;


                    }
                },
                {
                    x: 430,
                    y: 100,
                    speedY: 3,
                    width: 40,
                    height: 40,
                    color: "#ffcc11",
                    changeColor: function(color) {

                        this.color = color;


                    }
                }
            ]

            var player = {
                x: 20,
                y: 200,
                width: 50,
                height: 38,
                change: 3,
                color: "#ccff11",

                changeColor: function(color) {

                    this.color = color;

                },

                isMoving: false

            }

            //the goal object
            var goal = {
                x: 580,
                y: 200,
                width: 50,
                height: 36,
                color: "#ffff22",
            }

        

            var movePlayer = function() {
                player.isMoving = true;
            }

            var stopPlayer = function() {
                player.isMoving = false;
            }

            
             //event listeners to move player
            canvas.addEventListener('mousedown', movePlayer);

            canvas.addEventListener('mouseup', stopPlayer);

            canvas.addEventListener('touchstart', movePlayer);

            canvas.addEventListener('touchend', stopPlayer);
            
            //check the collision between two rectangles
            var checkCollision = function(rect1, rect2) {

                var closeOnWidth = Math.abs(rect1.x - rect2.x) <= Math.max(rect1.width, rect2.width);

                var closeOnHeight = Math.abs(rect1.y - rect2.y) <= Math.max(rect1.height, rect2.height);

                return closeOnWidth && closeOnHeight;
            }


            var step = function() {
                
                update();

                draw();

                window.requestAnimationFrame(step);

            }

            /***calculate game**/
            var update = function() {

                if (player.isMoving){
                   
                    player.x += player.change;
                
                }

                /*****added lesson 2*****/

                enemies.forEach(function(element, index) {

                    //check for collision with player
                    if (checkCollision(player, element)) {

                        gameLive = false;

                        //element.roar();

                        alert('Game Over!');

                        window.location = "";
                    }

                    //move enemy
                    element.y += element.speedY;

                    //check borders
                    if (element.y <= 10) {
                        element.y = 10;
                        //element.speedY = element.speedY * -1;
                        element.speedY *= -1;

                        element.changeColor("#ffcc11");

                    } else if (element.y >= GAME_HEIGHT - 50) {
                        element.y = GAME_HEIGHT - 50;
                        element.speedY *= -1;

                        element.changeColor("#ff1111");
                    }
                });


                if (checkCollision(player, goal)) {
                    //stop the game
                    gameLive = false;

                    alert('You\'ve won!');

                    window.location = "";

                }

                /*****end lesson 2*****/

                if (player.x < 20) {

                    player.x = 20;

                    player.change *= -1;

                    player.changeColor("#ccff11");
                }

                if (player.x > 580) {

                    player.x = 580;

                    player.change *= -1;

                    player.changeColor("#ffcc11");
                }

            }

            /***draw all board elemnts**/

            var draw = function() {

                ctx.clearRect(0, 0, 640, 360);

                // ctx.drawImage(sprites.bg, 0, 0);
                
                ctx.fillStyle = goal.color;

                ctx.fillRect(goal.x, goal.y, goal.width, goal.height);

                enemies.forEach(function(element, index) {

                    ctx.fillStyle = enemies[index].color;

                    ctx.fillRect(element.x, element.y, element.width, element.height);
                });

               

                ctx.drawImage(sprites.player, player.x, player.y);


            }



            loadAssets();

            step();

        });}