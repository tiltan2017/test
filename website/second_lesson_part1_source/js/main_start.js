var InitClass = InitClass || {};

InitClass.Init = function () {

    this.game = new Phaser.Game(800, 600, Phaser.AUTO, 'main_canvas', {
        init: init,
        preload: preload,
        create: create,
        update: update
    });

    function init() {

        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        this.game.physics.arcade.gravity.y = 700;

        this.cursors = this.game.input.keyboard.createCursorKeys();

        this.spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        this.RUNNING_SPEED = 250;
        
        this.JUMPING_SPEED = 550;

    }

    function preload() {

        //  37x45 is the size of each frame
        //  There are 18 frames in the PNG - you can leave this value blank if the frames fill up the entire PNG, but in this case there are some
        //  blank frames at the end, so we tell the loader how many to load

        var style = {
            font: "bold 32px Arial",
            fill: "#48ea48"
        };

        this.loadingText = this.game.add.text(350, 300, '0%', style);

        this.game.load.image('background', 'assets/bg.jpg');

        this.game.load.image('ground_tile', 'assets/ground_tile.png');

        this.game.load.image('door', 'assets/door.png');

        this.game.load.atlas('enemy', 'assets/spritesheets_ori/export-sheets/Enamy-Walk.png', 'assets/spritesheets_ori/export-sheets/Enamy-Walk.json');

        this.game.load.atlas('kitty', 'assets/spritesheets_ori/export-sheets/Kitty_Walk.png', 'assets/spritesheets_ori/export-sheets/Kitty_Walk.json');

        this.game.load.onFileComplete.add(updateProgressBar, this)

        this.game.load.text('levelData', './assets/levels/level1.json')


    }

    function updateProgressBar(progress, cacheID, success, filesloaded, totalfiles) {

        this.loadingText.text = progress + '%';
    }

    function create() {

        this.game.add.sprite(0, 0, 'background');

        this.levelData = JSON.parse(this.game.cache.getText('levelData'));

        this.tileData = this.levelData.tileData;

        this.door = this.game.add.sprite(this.levelData.goal.x, this.levelData.goal.y, 'door');
        
        this.game.physics.arcade.enable(this.door);

        this.door.body.allowGravity = false;

        this.door.body.immovable = true;

        this.door.body.collideWorldBounds = true;

        this.tileLayer = this.game.add.physicsGroup();

        for (var i = 0; i < this.tileData.length; i++) {

            var data = this.tileData[i];

            var startWidth = 0;

            var startHeight = 0;

            while (startHeight <= data.height) {

                while (startWidth < data.width) {

                    var t = this.game.add.sprite(data.x + startWidth, data.y + startHeight, 'ground_tile');

                    startWidth += 50;

                    this.tileLayer.add(t);

                    this.game.physics.arcade.enable(t);

                    t.body.allowGravity = false;

                    t.body.immovable = true;

                    t.body.collideWorldBounds = true;
                }

                startHeight += 50;

                startWidth = 0;

            }

        }

        /*** add enemies**/

        this.enemies = [];

        this.enemyData = this.levelData.enemyData;

        /*** add player**/

        this.player = this.game.add.sprite(140, 0, 'kitty');

        this.player.animations.add('walk');

        this.player.animations.play('walk', 20, true);

        this.player.anchor.setTo(0.5, 1);

        this.player.scale.x = -0.4;

        this.player.scale.y = 0.4;

        for (var i = 0; i < this.enemyData.length; i++) {

            var enemyData = this.enemyData[i];

            var enemy = this.game.add.sprite(enemyData.boundStartX, enemyData.boundStartY, 'enemy');

            enemy.animations.add('walk');

            enemy.animations.play('walk', 20, true);

            enemy.anchor.setTo(0.5,1);

            enemy.scale.x = -0.4;

            enemy.scale.y = 0.4;

            this.game.physics.arcade.enable(enemy);

            enemy.body.collideWorldBounds = true;

            this.enemies.push(enemy);

        }

        this.game.physics.arcade.enable(this.player);

        this.player.body.collideWorldBounds = true;


    }

    //  update isn't called until 'create' has completed. If you need to process stuff before that point (i.e. while the preload is still happening)
    //  then create a function called loadUpdate() and use that
    function update() {

        this.player.body.velocity.x = 0;


        if (this.cursors.left.isDown) {


            this.player.body.velocity.x -= this.RUNNING_SPEED;

            this.player.scale.x = 0.4;
        } else if (this.cursors.right.isDown) {
            this.player.body.velocity.x += this.RUNNING_SPEED;

            this.player.scale.x = -0.4;

        }

    
        this.game.physics.arcade.collide(this.player, this.tileLayer, hitGround);
        
        if (this.spaceKey.isDown && this.player.body.touching.down) {


            this.player.body.velocity.y -= this.JUMPING_SPEED;
        }

        
        for (var j = 0; j < this.enemies.length; j++) {

            var enemyData = this.enemyData[j];

            this.enemies[j].x += enemyData.speedX;
            
            this.game.physics.arcade.collide(this.enemies[j], this.tileLayer, hitGround);
            
            if (this.enemies[j].x > enemyData.boundEndX || this.enemies[j].x < enemyData.boundStartX) {

                enemyData.speedX *= -1;

                this.enemies[j].scale.x *= -1;


            } 

        }
        
        this.game.physics.arcade.collide(this.player, this.door, goToNextLevel);

    }

    function hitGround() {


    }
    
    function  goToNextLevel() {


        console.log("loading next level..")
        
        //this.game.start.start()
    }
    
    






}